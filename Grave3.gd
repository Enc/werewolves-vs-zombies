extends StaticBody2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func day():
	$Grave.show()
	$GraveMoonlit.hide()
	$Opengrave.show()
	$OpengraveMoonlit.hide()

func night():
	$Grave.hide()
	$GraveMoonlit.show()
	$Opengrave.hide()
	$OpengraveMoonlit.show()
