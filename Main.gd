extends Node

export (PackedScene) var Zombie

var ZOMBIE = load("Zombie.gd")
const SPAWNING_TIME = 6
const FLEE = "Run!"
const HUNT = "Get 'em!"
var score
var day = true
var day_num = 1

func _ready():
	randomize()
	
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func new_game():
	score = 0
	$HUD.update_score(score)
	$HUD.show_message(FLEE)
	$Player.start($StartPosition.position)
	$StartTimer.start()
	$ThemeDay.play()
	$ThemeNight.play()
	$ThemeDay.volume_db = 0
	$ThemeNight.volume_db = -80

func _on_MobTimer_timeout():
	_spawn_zombie()
	$MobTimer.wait_time = SPAWNING_TIME as float / day_num as float

func _spawn_zombie():
	var zombie = Zombie.instance()
	add_child(zombie)
	match randi() % 2:
		0:
			zombie.position = $SpawnPoint1.position
		1:
			zombie.position = $SpawnPoint2.position
	zombie.rotation = rand_range(-2*PI, 2*PI)

func game_over():
	$ScoreTimer.stop()
	$MobTimer.stop()
	$DayTimer.stop()
	$HUD.show_game_over()
	get_tree().call_group("mobs", "queue_free")

func _on_StartTimer_timeout():
	$ScoreTimer.start()
	$DayTimer.start()
	$MobTimer.wait_time = 1
	$MobTimer.start()
	
func _on_ScoreTimer_timeout():
	score += 1
	$HUD.update_score(score)

func _on_DayTimer_timeout():
	$Player.start_transform()
	$TransformationTimer.start()
	day = !day
	match day:
		true:
			$HUD.show_message(FLEE)
			$Graveyard.show()
			$GraveyardNight.hide()
			$Grave3.day()
			$TallGrave1.day()
			$OpenGrave2.day()
			$Mausoleum.day()
			day_num += 1
			$ThemeDay.volume_db = 0
			$ThemeNight.volume_db = -80
		false:
			$HUD.show_message(HUNT)
			$Graveyard.hide()
			$GraveyardNight.show()
			$Grave3.night()
			$TallGrave1.night()
			$OpenGrave2.night()
			$Mausoleum.night()
			$ThemeDay.volume_db = -80
			$ThemeNight.volume_db = 0


func _on_TransformationTimer_timeout():
	$TransformationTimer.stop()
	$Player.complete_transform()
