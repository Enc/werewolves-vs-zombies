extends KinematicBody2D

const PLAYER = preload("Player.gd")
const SPEED = 200

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func _physics_process(delta):
	var velocity = Vector2(1, 0).rotated(rotation) * SPEED
	var collision = move_and_collide(velocity * delta)
	if collision:
		velocity = velocity.bounce(collision.normal)
		rotation = velocity.angle()
		var collider = collision.collider
		if collider.has_method("hit"):
			collider.hit()
		if collider is PLAYER  && !collider.transforming:
			if collider.werewolf:
				die()
			else:
				collider.die()  # Player disappears after being hit.

func die():
	hide()
	$CollisionShape2D.set_deferred("disabled", true)

func is_zombie():
	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
