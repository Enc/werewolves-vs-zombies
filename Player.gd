extends KinematicBody2D

signal hit

const SPEED = 400  # How fast the player will move (pixels/sec).
var screen_size  # Size of the game window.
var werewolf = false
var transforming = false

# Called when the node enters the scene tree for the first time.
func _ready():
	screen_size = get_viewport_rect().size
	$AnimatedSprite.animation = "human"

func _physics_process(delta):
	if transforming:
		$AnimatedSprite.play()
	else:
		var velocity = Vector2()  # The player's movement vector.
		if Input.is_action_pressed("ui_right"):
			velocity.x += 1
		if Input.is_action_pressed("ui_left"):
			velocity.x -= 1
		if Input.is_action_pressed("ui_down"):
			velocity.y += 1
		if Input.is_action_pressed("ui_up"):
			velocity.y -= 1
		if velocity.length() > 0:
			velocity = velocity.normalized() * SPEED
			rotation = velocity.angle()
			$AnimatedSprite.play()
		else:
			$AnimatedSprite.stop()
		var collision = move_and_collide(velocity * delta)
		if collision:
			var collider = collision.collider
			if collider.has_method("hit"):
				collider.hit()
			if collider.has_method("is_zombie"):
				if werewolf:
					collider.die()
				else:
					die()

func start(pos):
	position = pos
	show()
	$CollisionShapeHuman.set_deferred("disabled", false)
	$CollisionShapeWolf.set_deferred("disabled", true)

func die():
	hide()  # Player disappears after being hit.
	emit_signal("hit")
	$CollisionShapeHuman.set_deferred("disabled", true)
	$CollisionShapeWolf.set_deferred("disabled", true)
	$Death.play()

func start_transform():
	transforming = true
	werewolf = !werewolf
	if werewolf:
		$AnimatedSprite.animation = "into_wolf"
	else:
		$AnimatedSprite.animation = "into_human"		
	$Transformation.play()
	$CollisionShapeHuman.set_deferred("disabled", false)
	$CollisionShapeWolf.set_deferred("disabled", false)

func complete_transform():
	transforming = false
	if werewolf:
		$AnimatedSprite.animation = "wolf"
		$CollisionShapeHuman.set_deferred("disabled", true)
	else:
		$AnimatedSprite.animation = "human"
		$CollisionShapeWolf.set_deferred("disabled", true)

func _on_AnimatedSprite_frame_changed():
	if !(transforming && $CollisionShapeHuman.disabled && $CollisionShapeWolf.disabled):
		match randi() % 9 + 1:
			1:
				$Steps1.play()
			2:
				$Steps2.play()
			3:
				$Steps3.play()
			4:
				$Steps4.play()
			5:
				$Steps5.play()
			6:
				$Steps6.play()
			7:
				$Steps7.play()
			8:
				$Steps8.play()
			9:
				$Steps9.play()
